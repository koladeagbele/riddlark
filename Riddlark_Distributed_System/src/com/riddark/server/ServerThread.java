package com.riddark.server;

import com.riddark.Constants;
import com.riddark.client.User;
import com.riddark.request.RequestOperation;

import java.io.*;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;


public class ServerThread extends Thread {
    private static final ConcurrentHashMap<Integer, User> userDB = new ConcurrentHashMap<>();
    private final ServerAuthenticationLogic serverAuthenticationLogic = new ServerAuthenticationLogic(userDB);
    private Socket socket;

    public ServerThread() {

    }

    public ServerThread(Socket socket) {
        this.socket = socket;
    }

    public void run() {
        try {
            InputStream is = socket.getInputStream();
            ObjectInputStream ois = new ObjectInputStream(is);
            RequestOperation user = (RequestOperation) ois.readObject();
            String reply = "";
            boolean isUserAvailable = serverAuthenticationLogic.userNameIsAvailable(user.getUser());
            boolean isAuthenticated;
            System.out.println("Client with username " + user.getUser().getUsername()
                    + " is attempting to login / register:");

            if (user.getOperationType() == Constants.LOGIN_NUM) {
                isAuthenticated = serverAuthenticationLogic.userAuthentication(user.getUser());

                if (isAuthenticated && serverAuthenticationLogic.checkUserNameAndPswLength(user.getUser())) {
                    reply = "User " + user.getUser().getUsername() + " successfully Logged in!";
                } else {
                    reply = "User " + user.getUser().getUsername() + " not authenticated login failed!";
                }
                System.out.println(reply);
            }
            if (user.getOperationType() == Constants.SIGN_UP_NUM) {
                if (isUserAvailable && serverAuthenticationLogic.checkUserNameAndPswLength(user.getUser())) {
                    userDB.put(userDB.size() + 1, user.getUser());
                    reply = "User name " + user.getUser().getUsername() + " registration successful!";
                } else {
                    reply = "User name " + user.getUser().getUsername() + " already exist, registration failed!";
                }
                System.out.println(reply);
            }

            System.out.println();
            OutputStream os = socket.getOutputStream();
            BufferedWriter oos = new BufferedWriter(new OutputStreamWriter(os));
            //Output to the client response
            oos.write(reply);
            oos.flush();

            closeAllConnections(is, ois, os, oos);
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void closeAllConnections(InputStream is, ObjectInputStream ois,
                                     OutputStream os, BufferedWriter oos) throws IOException {
        oos.close();
        os.close();
        ois.close();
        is.close();
    }


}