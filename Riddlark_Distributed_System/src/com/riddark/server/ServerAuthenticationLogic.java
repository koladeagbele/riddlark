package com.riddark.server;

import com.riddark.client.User;

import java.util.concurrent.ConcurrentHashMap;

public class ServerAuthenticationLogic {
    private  ConcurrentHashMap<Integer, User> userDB;

    public ServerAuthenticationLogic(ConcurrentHashMap<Integer, User> userDB) {
        this.userDB = userDB;
    }

    protected boolean userAuthentication(User user) {

        for(int i =  1; i <= userDB.size(); i++) {
            if(userDB.isEmpty()) {
                break;
            }
            User _user = userDB.get(i);
            String userPassword = user.getPassword();
            String userName = user.getUsername();
            if(userName.equals(_user.getUsername()) && userPassword.equals(_user.getPassword())) {
                return true;
            }
        }
        return false;
    }

    protected boolean userNameIsAvailable(User user) {
        boolean isUserNameAvailable = true;
        for(int i =  1; i <= userDB.size(); i++) {
            if(userDB.isEmpty()) {
                break;
            }
            User _user = userDB.get(i);
            String userName = _user.getUsername();
            if(user.getUsername().equals(userName)) {
                isUserNameAvailable = false;
            }
        }
        return isUserNameAvailable;

    }

    protected boolean checkUserNameAndPswLength(User user) {
        return user.getUsername().length() >= 4 && user.getPassword().length() >= 4;
    }
}
