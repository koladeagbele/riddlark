package com.riddark.server;

import com.riddark.Constants;
import com.riddark.client.User;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) {
        System.out.println(Constants.WELCOME_MESSAGE+ " Server");
        System.out.println("Server start on "+ Constants.PORT);
        runServer();
    }

    private static void runServer() {
        ServerSocket server;
        Socket socket;

        try {
            server=new ServerSocket(Constants.PORT);
            while(true){
                socket=server.accept();
                ServerThread st=new ServerThread(socket);
                st.start();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}