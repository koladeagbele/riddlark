package com.riddark;

public class Constants {
    public static final int PORT = 9001;
    public static final String ADDRESS = "localhost";
    public static final String SIGN_UP = "sign_up";
    public static final String LOGIN = "login";
    public static final int LOGIN_NUM = 2;
    public static final int SIGN_UP_NUM = 1;
    public static final String MSG1 = "Which operation do you want to perform? ";
    public static final String MSG2 = "1. Register a user name?";
    public static final String MSG3 = "2. Login to your account?";
    public static final String MSG4 = "*** Please ensure to register a user account first before attempting to login. ***";
    public static final String ENTER_USER = "Enter a username: ";
    public static final String ENTER_PSW = "Enter a password: ";
    public static final String WELCOME_MESSAGE = "Welcome to Riddlark Game";
    public static final String SELECT_OPS = "Select an operation now!!";
    public static final String TEAM_PREFIX = "team_";
    public static final int MAX_NUM_MEMBER = 4;
}
