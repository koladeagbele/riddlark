package com.riddark.request;

import com.riddark.client.User;

import java.io.Serializable;

public class RequestOperation implements Serializable {
    private static final long serialVersionUID = 1001L;
    private int operationType;
    private User user;

    public RequestOperation(int operationType) {
        this.operationType = operationType;
    }

    public RequestOperation(int operationType, User user) {
        this.operationType = operationType;
        this.user = user;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public int getOperationType() {
        return operationType;
    }

    public void setOperationType(int operationType) {
        this.operationType = operationType;
    }
}
