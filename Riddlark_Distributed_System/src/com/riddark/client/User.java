package com.riddark.client;


import java.io.Serializable;

public class User implements Serializable {

    private static final long serialVersionUID = 1000L;

    private String username;
    private String password;
    private int teamID;

    public User() {
    }

    public User (String username, String password) {
        this.username = username;
        this.password = password;
    }

    public User(String username, String password, int teamID) {
        this.username = username;
        this.password = password;
        this.teamID = teamID;
    }

    public int getTeamID() {
        return teamID;
    }

    public void setTeamID(int teamID) {
        this.teamID = teamID;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
