package com.riddark.client;

import com.riddark.Constants;
import com.riddark.request.RequestOperation;
import com.sun.istack.internal.NotNull;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    private Socket socket = null;
    private OutputStream os = null;
    private ObjectOutputStream oos = null;

    public static void main(String[] args) {
        System.out.println(Constants.WELCOME_MESSAGE);
        Client client = new Client();

        System.out.println(Constants.MSG1);
        System.out.println(Constants.MSG2);
        System.out.println(Constants.MSG3);
        System.out.println(Constants.MSG4);
        Scanner sc = new Scanner(System.in);
        while(true) {
            System.out.println("*********************");
            System.out.println(Constants.SELECT_OPS);
            int operation = sc.nextInt();
            RequestOperation requestOperation = new RequestOperation(operation);
            System.out.print(Constants.ENTER_USER);
            String userName = sc.next();

            System.out.print(Constants.ENTER_PSW);
            String userPassword = sc.next();
            RequestOperation sendData = new RequestOperation(requestOperation.getOperationType(),
                    new User(userName, userPassword));
            client.executeOperation(sendData);
        }
    }

    private void loginUser(RequestOperation requestOperation) {
        try {
            setUpConnection();
            //Output is the value of an object
            oos.writeObject(requestOperation);
            //Read the response returned by the server
            BufferedReader br = serverResponse();
            String reply;
            while ((reply = br.readLine()) != null) {
                System.out.println("Server response :" + reply);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setUpConnection() throws IOException {
        socket = new Socket(Constants.ADDRESS, Constants.PORT);
        //Get output flow object
        os = socket.getOutputStream();
        oos = new ObjectOutputStream(os);
    }

    private void signUpUsers(RequestOperation requestOperation) {
        try {
            setUpConnection();
            oos.writeObject(requestOperation);
            //Read the response returned by the server
            BufferedReader br = serverResponse();
            String reply;
            while ((reply = br.readLine()) != null) {
                System.out.println("Server says:" + reply);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @NotNull
    private BufferedReader serverResponse() throws IOException {
        InputStream is = socket.getInputStream();
        return new BufferedReader(new InputStreamReader(is));
    }

    private void executeOperation(RequestOperation requestOperation) {
        switch (requestOperation.getOperationType()) {
            case Constants.SIGN_UP_NUM:
                signUpUsers(requestOperation);
                break;
            case Constants.LOGIN_NUM:
                loginUser(requestOperation);
                break;
        }
    }
}

