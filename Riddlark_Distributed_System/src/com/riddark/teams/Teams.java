package com.riddark.teams;

import com.riddark.Constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

public class Teams {
    private int teamId;
    private String newTeamName;
    private static final ConcurrentHashMap<Integer, Teams> teamDB = new ConcurrentHashMap<>();
    private final HashMap<Integer, TeamSize> readers = new HashMap<>();

    public Teams(int teamId, String newTeamName) {
        this.teamId = teamId;
        this.newTeamName = newTeamName;
    }

    private static Teams createTeam() {
        synchronized (teamDB) {
            int numberOfTeams = teamDB.size();
            int teamId = numberOfTeams + 1;
            String newTeamName = Constants.TEAM_PREFIX + teamId;
            Teams newTeam = new Teams(teamId, newTeamName);
            teamDB.put(teamId, newTeam);
            return newTeam;
        }
    }
    private ArrayList<Integer> getAvailableTeams() {
        ArrayList<Integer> availableTeams = new ArrayList<>();
        teamDB.forEach((k, v) -> {
            if(!v.isFull()) {
                availableTeams.add(v.teamId);
            }
        });
        return availableTeams;
    }

    public boolean isFull() {
        return readers.size() >= Constants.MAX_NUM_MEMBER;
    }
}
